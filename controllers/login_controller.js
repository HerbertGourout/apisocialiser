let userModel = require('../models/user_model');
let jwtFactory = require('jsonwebtoken');
let properties = require('../config/properties.js');


exports.login = function(req, res) {
  console.log(req);


  const email = req.body.email;
  const pass = req.body.password;

  if (email == undefined || email.trim() == '' || pass == undefined) {
    res.status(400).json({
      success: false,
      message: 'Incorrect input, missing email and/or password.'
    });
    return;
  }

 
  userModel.findOne({ email: email.trim(), password: pass }, (err, data) => {
   
    if (data) {
      
      const JWToken = jwtFactory.sign(
        {
          id: data._id,
          name: data.lastName,
          email: data.email,
          isAdmin: data.isAdmin
        },
        properties.tokenKey,
        { expiresIn: 1200 }
      );

      
      res.json({
        JWT: JWToken,
        success: true,
        user: data.firstName,
        nom: data.firstName,
        prenom: data.lastName,
        email: data.email,
        id: data._id,
        expiresIn: 1200,
        message: 'Successfully logged in.'
      });
    } else {
     
      res.status(404).json({
        success: false,
        message: 'Login or password unknown/incorrect.'
      });
    }
  });
};


exports.createUser = function(req, res) {
  console.log(req.body);
 
  const lastName = req.body.lastName;
  const firstName = req.body.firstName;
  const email = req.body.email;
  const password = req.body.password; 
  const confirmPassword = req.body.confirmPassword;

  console.log(
    email +
      ' == ' +
      password +
      ' == ' +
      confirmPassword +
      ' == ' +
      lastName +
      ' == ' +
      firstName
  );

  if (
    lastName == undefined ||
    firstName == undefined ||
    email == undefined ||
    password == undefined ||
    confirmPassword == undefined
  ) {
    res.status(400).json({
      success: false,
      message: 'Incorrect input, missing required field(s).'
    });
    return;
  } else if (
    lastName.trim() == '' ||
    firstName.trim() == '' ||
    email.trim() == '' ||
    email.indexOf('@') == -1
  ) {
    res.status(400).json({
      success: false,
      message: 'Incorrect input, please provide all fields.'
    });
    return;
  } else if (password.trim().length < 4) {
    res.status(400).json({
      success: false,
      message: 'Password must have a minimal size of 4 caracters.'
    });
    return;
  } else if (password != confirmPassword) {
    res
      .status(400)
      .json({ success: false, message: 'Both passwords are different.' });
    return;
  }


  userModel.findOne({ email: email, password: password }, (err, data) => {
    
    if (data)
      res.status(400).json({
        success: false,
        message: 'This user already exist, please try another one.'
      });
    
    else
      new userModel({
        lastName,
        firstName,
        email,
        password
      }).save((error, user) => {
        if (error) console.error(error);
        console.log(user);
        res.json(user);
      });
  });
};
