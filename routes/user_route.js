let app             = require('express').Router();
let user_controller = require('../controllers/user_controller')


app.route('/')

   .get(user_controller.getUser)

   .put(user_controller.updateUser)

app.route('/:id')

   .get(user_controller.getUser)

module.exports = app;