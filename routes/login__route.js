let app = require('express').Router();
let login_controller = require('../controllers/login_controller')

app.route('/signin')
   .post(login_controller.login)


app.route('/signup')
   .post(login_controller.createUser)

module.exports = app;