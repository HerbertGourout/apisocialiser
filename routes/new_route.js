let app = require('express').Router();
let news_controller = require('../controllers/news_controller')


app.route('/')

   .get(news_controller.getNews)

   .post(news_controller.createNews)

   .put(news_controller.updateNews)

   .delete(news_controller.deleteNews)

app.route('/:id')

   .get(news_controller.getNews)

module.exports = app;
