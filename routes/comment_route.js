let app = require('express').Router();
let comment_controller = require('../controllers/comment_controller')

app.route('/')

   .get(comment_controller.getComment)


   .post(comment_controller.createComment)


   .put(comment_controller.updateComment)


   .delete(comment_controller.deleteComment);

module.exports = app;


