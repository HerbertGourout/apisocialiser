
let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let cors = require('cors');

let auth = require('./utils/auth');

let user_route = require('./routes/user_route.js');
let news_route = require('./routes/news_route.js');
let login_route = require('./routes/login_route.js');
let comment_route = require('./routes/comment_route.js');
const PORTE = process.env.PORT || 4300;

let app = initServer(PORTE);


app.use('/auth', login_route); 
app.use('/rest/User', auth.validSession, user_route); 
app.use('/rest/News', auth.validSession, news_route); 
app.use('/rest/Comment', auth.validSession, comment_route); 


function initServer(port) {
  console.log('Starting Express Server...');
  let app = express();

  const mongodb_config = require('./config/mongo_config');
  mongoose
    .connect(mongodb_config.databaseUrl, { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err));

  console.log('MongoServer connected to ' + mongodb_config.databaseUrl);
  console.log('Enabling other domains access...');

  app.use(cors());

  console.log('Ok');

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.listen(port);
  console.log('Listening on port ' + port);

  console.log('Server Started !');

  initSwagger(app);

  return app;
}


function initSwagger(app) {
  var swaggerJSDoc = require('swagger-jsdoc');

 
  var swaggerDefinition = {
    info: {
      title: 'Node Swagger API',
      version: '1.0.0',
      description: 'Demonstrating how to describe a RESTful API with Swagger'
    },
    host: 'localhost:4300',
    basePath: '/'
  };

  
  var options = {
   
    swaggerDefinition: swaggerDefinition,
   
    apis: ['./routes/*.js']
  };


  var swaggerSpec = swaggerJSDoc(options);


  app.get('/api-docs', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
}

module.exports = app;