let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = mongoose.model('Comment', new Schema({
    content: {type: String, required: 'champs indispenable'},
    author:  {type: String, required: 'champs indispenable'},
    newsId:  {type: String, required: 'champs indispenable'},
    created: {type: Date, default: new Date()}
}))