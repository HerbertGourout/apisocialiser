let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = mongoose.model('News', new Schema({
    title:       {type: String, required: 'indispensable'},
    description: {type: String, required: 'indispensable'},
    created:     {type: Date,   default: new Date()},
    author:      {type: String, required: 'indispensable'}
}))