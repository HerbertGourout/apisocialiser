let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = mongoose.model('User', new Schema({
    lastName:   {type: String,  required: 'indispensable'},
    firstName:  {type: String,  required: 'indispensable'},
    email:      {type: String,  required: 'indispensable'},
    isAdmin:    {type: Boolean, default: false},
    created:    {type: Date,    default: Date.now},
    password:   {type: String,  required: 'indispensable'}
}))