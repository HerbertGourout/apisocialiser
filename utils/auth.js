let jwtFactory = require('jsonwebtoken');
let userModel  = require('../models/user_model')
let properties = require('../config/properties.js')

module.exports = {

    ADMIN: "ADMIN",
    USER: "USER",

    /**
     * 
     * @param {*} JWToken 
     * @param {*} expectedRole 
     */
    validateCurrentUser(JWToken = String, expectedRole = String){
        
       
        return jwtFactory.verify(userIdFromJWToken, properties.tokenKey, (err, decodedToken) => {

            if(!decodedToken)
                return false;

            
            return userModel.findOne(decodedToken.payload.id, (err, user) => {

               
                if(user){
                    return validateAccess(user.roles, expectedRole);
                }
                else {
                    return false;
                }

            });
        }); 
    },

    /**
     * 
     * @param {*} userRole 
     * @param {*} expectedRole 
     */
    validateAccess(expectedRole = String){

        if(userRole == undefined || userRoles.length == 0 || expectedRole == undefined || expectedRole.trim().equals(""))
            return false;

        return userRole.contains(expectedRole);
    },

    getCurrentUser(req){

        let cToken = req.headers['authorization'];

        cToken = cToken.split(" ")[1] || req.headers['x-access-token'];

       
        return jwtFactory.verify(cToken, properties.tokenKey);
    },

    /**
     *
     * @param {*} userRole 
     */
    validateAdmin(req){
        return this.getCurrentUser(req).isAdmin;
    },

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    validSession(req, res, next){

        
        let cToken = req.headers['authorization'];

        
        if(!cToken)
            res.status(400).json({status: "error", message: "Missing JWToken for authentication, please login."})

        cToken = cToken.split(" ")[1] || req.headers['x-access-token'];

        
        jwtFactory.verify(cToken, properties.tokenKey, (err, decodedToken) => {

            
            if(err){
              
                res.status(401).json({status: "error", message: err.message});
            }
         
            else {
                next();
            }
        });
    }
}